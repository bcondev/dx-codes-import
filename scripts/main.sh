#!/bin/bash

printUsage () {
	echo "Usage  : ${SCRIPTNAME} <SCHEMA> <INPUT XML Tabular ICD-10 file> [host] [user] [password]"
	echo "Example: ${SCRIPTNAME} ag_prod icd10cm_tabular_2024.xml"
}
doerror () {
	# param 1 is rc, 2 is error message
	echo "${2}"
	printUsage
	exit ${1}
}

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
DATETIMESTAMP=`date '+%m-%d-%Y %H:%M:%S'`
PID=$$
SCRIPTNAME=$0
SCHEMA=$1
DATAFILE=$2
HOST=$3
USER=$4
PASSWORD=$5


[ -z "${SCHEMA}" ] && doerror 1 "Schema name is required"
[ -z "${DATAFILE}" ] && doerror 1 "Input row-base data file (CSV, '|' delimited, etc..) is required."
if [ -z "${PASSWORD}" ]; then
	PASSWORD=`grep -i password ~/.my.cnf | sed 's/"//g' | cut -d'=' -f 2` 
fi
if [ -z "${HOST}" ]; then
	HOST=localhost
fi
if [ -z "${USER}" ]; then
	USER=root
fi

BINNAME=`find ${SCRIPT_DIR}/.. -name "dx_codes*dep*.jar"`
java -jar ${BINNAME}  -i "${DATAFILE}"  -s ${SCHEMA} -h ${HOST} -u ${USER} -p ${PASSWORD}
