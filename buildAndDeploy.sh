#!/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

VERSION=$1
DESTDIR=$2
if [ -z ${VERSION} ] || [ -z ${DESTDIR} ]; then
	echo "Usage: $0 <version> <deployment dir> (i.e. $0 1.2.0 /common-services/cms-dx-import)"
	exit 1
fi

DEPLOYDIR=${DESTDIR}_v${VERSION}
if [ -d ${DEPLOYDIR} ]; then
	echo "Directory already exists: ${DEPLOYDIR}"
	exit 1
fi

#build it
cd $SCRIPT_DIR
mvn clean; mvn compile; mvn package

if [ $? -eq 0 ]; then
	mkdir -p $DEPLOYDIR/bin
	cp -pr scripts src/test/input $DEPLOYDIR
	cp -pr target/*dep*.jar $DEPLOYDIR/bin
	echo "Build successful.  Deploy dir: $DEPLOYDIR"
else
	echo "Build failed."
fi
