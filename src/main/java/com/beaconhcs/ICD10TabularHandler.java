/* <BHCS_COPYRIGHT_NOTICE>
 * This file contains proprietary information of Beacon HCS.
 * Copying or reproduction without prior written approval is prohibited.
 * All rights reserved
 * Copyright (c) 2022
 * Author:  Lu Nguyen (lnguyen@beaconhcs.com)
 * </BHCS_COPYRIGHT_NOTICE>
 */
package com.beaconhcs;

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

import java.util.HashMap;
import java.util.Map;

public class ICD10TabularHandler extends DefaultHandler  {

	private StringBuilder currentContent;
	private String currentName;
	private final HashMap<String, String> dxCodesMap;
	private int level = 0;

	public void setCurrentName(String currentName) {
		this.currentName = currentName;
	}

	DB db;
	
	public ICD10TabularHandler(final DB db, final HashMap<String, String> dxCodesMap)
	{
		this.db = db;
		this.dxCodesMap = dxCodesMap;
	}
	
	@Override
	// Called at the beginning of a document.
	public void startDocument()	{
	}
	
	@Override
	// Called at the end of a document.
	public void endDocument() {
	}

	@Override
	// Called when a DTD is present and ignorable whitespace is encountered.
	public void ignorableWhitespace( char[] ch, int start, int length) {
		
	}
	
	@Override
	// Called when a processing instruction is recognized.
	public void processingInstruction(String target, String data) {
		
	}
	
	@Override
	// Called when an unresolved entity is encountered.
	public void skippedEntity(String name) {
		
	}
	

	@Override
	// Called when a new namespace mapping is defined.
	public void startPrefixMapping(String prefix, String uri) {
		
	}

	@Override
	// Called when a namespace definition ends its scope.
	public void endPrefixMapping(String prefix) {
		
	}


	@Override
	// Provides a Locator that can be used to identify positions in the document.
	public void setDocumentLocator(Locator locator) {
		
	}
	
	@Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)  throws SAXException {
		currentContent = new StringBuilder();
		if (localName.equals("diag")) level ++;
   	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (localName.equals("diag")) {
			setCurrentName("");
			level--;
		} else if (localName.equals("name")) {
			setCurrentName(currentContent.toString());
		} else if (localName.equals("desc")) {
			if (level > 0 && currentContent.length() != 0 && !currentName.isEmpty())
				dxCodesMap.put(this.currentName,  currentContent.toString());
		}

	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		currentContent.append(new String(ch, start, length));
	}
}   																																																																																																																																																																																																																																																																																																																																																																																																																																																																																						
