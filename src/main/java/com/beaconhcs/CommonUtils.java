/* <BHCS_COPYRIGHT_NOTICE>
 * This file contains proprietary information of Beacon HCS.
 * Copying or reproduction without prior written approval is prohibited.
 * All rights reserved
 * Copyright (c) 2022
 * Author:  Lu Nguyen (lnguyen@beaconhcs.com)
 * </BHCS_COPYRIGHT_NOTICE>
 */
package com.beaconhcs;
import org.apache.commons.cli.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;

public class CommonUtils {
    public static boolean checkRequiredFile(CommandLine cmd, String arg, String fileDesc)
    {
            try
            {
                    if(!cmd.hasOption(arg)) {
                            throw new RuntimeException("Option '" + arg + "' was not provided.");
                    }
                    else {
                        // validate the input file
                            String inputFileName = cmd.getOptionValue(arg);
                            File inputFile = new File(inputFileName);
                        if (!inputFile.exists()) {

                            throw new RuntimeException(fileDesc + " does not exists.");
                        }
                        if (!inputFile.canRead()) {
                            throw new RuntimeException(fileDesc + " is not readable.  Check permissions.");
                        }
                    }
                    return true;
            }
            catch (RuntimeException e) {
                    System.err.println("Error: " + e.getMessage());
                    return false;
            }
    }

}
