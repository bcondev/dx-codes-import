/* <BHCS_COPYRIGHT_NOTICE>
 * This file contains proprietary information of Beacon HCS.
 * Copying or reproduction without prior written approval is prohibited.
 * All rights reserved
 * Copyright (c) 2022
 * </BHCS_COPYRIGHT_NOTICE>
 */
package com.beaconhcs;

import org.apache.commons.cli.*;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import static java.lang.System.exit;

public class ImportDXMain {

    public static void main(String[] args) {

        Options options = new Options();
        options.addRequiredOption("i", "inputfile", true, "Input DX input XML filename.");
        options.addRequiredOption("u", "user", true, "User ID");
        options.addRequiredOption("p", "password", true, "Password");
        options.addRequiredOption("s", "schema", true, "Database schema name (i.e. ag_prod)");
        options.addOption("h", "host", true, "Host (defaults to localhost)");
        options.addOption("t", "port", true, "Port (defaults to 3306)");

        try {

            CommandLineParser parser = new DefaultParser();
            CommandLine cmd = parser.parse(options, args);

            String inputFileName = cmd.getOptionValue("i");
            String userID = cmd.getOptionValue("u");
            String password = cmd.getOptionValue("p");
            String host = cmd.getOptionValue("h");
            String port = cmd.getOptionValue("t");
            String schema = cmd.getOptionValue("s");

            if (host == null || host.isEmpty()) host = "localhost";
            if (port == null || port.isEmpty()) port = "3306";
            //assertEquals("Port must be an int", Integer.MAX_VALUE, NumberUtils.parseNumber(port, Integer.class).intValue());

            DB db = new DB(userID, password, host, port, schema);;

            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setNamespaceAware(true);
            SAXParser saxParser = spf.newSAXParser();

            HashMap<String, String> dxCodesMap = new HashMap<String, String>();
            HashMap<String, String> existingdxCodesMap = new HashMap<String, String>();
            ICD10TabularHandler handler = new ICD10TabularHandler(db, dxCodesMap);
            XMLReader xmlReader = saxParser.getXMLReader();
            xmlReader.setContentHandler(handler);
            xmlReader.parse(inputFileName);

            db.loadICD10(existingdxCodesMap);
            db.updateICD10(dxCodesMap, existingdxCodesMap);
            exit (0);
        }
        catch (ParseException e) {
            System.err.println("Error message: " + e.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("dx-codes-import", options);
        }
        catch (RuntimeException e) {
            System.err.println("Runtime error: " + e.getMessage());
        } catch (ParserConfigurationException e) {
            System.err.println("Parse config error: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("IO Exception error: " + e.getMessage());
            e.printStackTrace();
        } catch (SAXException e) {
            System.err.println("SAX Parsing rror: " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            System.err.println("Exception: " + e.getMessage());
        }
        exit  (1);
    }
}
