/* <BHCS_COPYRIGHT_NOTICE>
 * This file contains proprietary information of Beacon HCS.
 * Copying or reproduction without prior written approval is prohibited.
 * All rights reserved
 * Copyright (c) 2022
 * Author:  Lu Nguyen (lnguyen@beaconhcs.com)
 * </BHCS_COPYRIGHT_NOTICE>
 */

package com.beaconhcs;


import java.awt.color.ICC_ColorSpace;
import java.net.URLEncoder;
import java.sql.*;
import java.util.HashMap;

public class DB
{
	private Connection db;
	private Statement statement;
	private boolean autoCommit = true;
	private boolean initOk = false;

    public DB(final String userID, final String password, final String host, final String port, final String schema)
    {
        connect(userID, password, host, port, schema);
    }

    public void  connect(final String userID, final String password, final String host, final String port, final String schema) throws RuntimeException
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            String url = "jdbc:mysql://" + host + ":" + port + "/" + schema + "?autoReconnect=true&useSSL=false&requireSSL=false";
            String encodedPassword = URLEncoder.encode(password, "UTF-8");
            db = DriverManager.getConnection(url, userID, password);

            /*
            url = "jdbc:mysql://" + host + ":" + port + "/" + schema + "?user=" + userID + "&password=" +
                    URLEncoder.encode(password, "UTF-8") + "&useSSL=false";
            System.out.println("Connect string: " + url + " pw: " + password + " encodedpw: " + encodedPassword);
            db = DriverManager.getConnection(url);
            */

            initOk = true;
            db.setAutoCommit(true);
        } catch (SQLException e) {
            throw new RuntimeException("Unable to connect to the database.  Error message: " + e.getMessage());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void updateICD10Record(String dxCode, String desc, String whereClauseDxCode)
    {
        try {
            String query = " update diagnosis_code set updt_dt = now(), description = ?,  dx_code = ? " +
                    "where dx_code = ? and icd_type ='ICD-10'";
            PreparedStatement preparedStmt = db.prepareStatement(query);
            preparedStmt.setString (1, desc);
            preparedStmt.setString (2, dxCode);
            preparedStmt.setString (3, whereClauseDxCode);

            preparedStmt.execute();
            preparedStmt.close();;
        } catch (SQLException e){
            throw new RuntimeException("Unable to update existing code: " + dxCode);
        }
    }

    private void insertICD10Record(String dxCode, String desc)
    {
        try {
            String query = " insert into  diagnosis_code (active, insrt_dt, updt_dt, description," +
                    "dx_code, default_value, sort, start_dt, system, icd_type)"
                    + " values ('Y', now(), now(), ?, ?,'N', 0, now(), 'N', 'ICD-10')";

            PreparedStatement preparedStmt = db.prepareStatement(query);
            preparedStmt.setString (1, desc);
            preparedStmt.setString (2, dxCode);

            // execute the preparedstatement
            preparedStmt.execute();
            preparedStmt.close();;
        } catch (SQLException e){
            throw new RuntimeException("Unable to insert new DX code: " + dxCode);
        }
    }

    public void updateICD10(HashMap<String, String> dxCodesMap, HashMap<String, String> existingdxCodesMap)
    {
        int brandNew = 0, revCount = 0, unRevCount = 0;
        // THese wo variables track to see if we have duplicates in the DB.  For now, just informational only.
        for (HashMap.Entry<String, String> entry : dxCodesMap.entrySet()) {
            String dxCode = entry.getKey();
            String dxCodeNoDot = dxCode.replace(".", "");
            String desc = entry.getValue();
            //System.out.println ("Code:" + dxCode + " desc: "+ desc);
            String existingDesc = existingdxCodesMap.get(dxCode);
            boolean standardDxCodeNotFound = false;
            if (existingDesc == null) {
                standardDxCodeNotFound = true;
                existingDesc = existingdxCodesMap.get(dxCodeNoDot);
            }
            // Never seen before, just add it.
            if (existingDesc == null)
            {
                brandNew ++;
                insertICD10Record(dxCode, desc);
            }
            else if (!desc.equals(existingDesc) || standardDxCodeNotFound) {
                revCount ++;
                if (standardDxCodeNotFound)
                    updateICD10Record(dxCode, desc, dxCodeNoDot);
                else
                    updateICD10Record(dxCode, desc, dxCode);

            }
            else unRevCount++;
        }
        System.out.println("Import Summary:");
        System.out.println("Inserted " + brandNew + " new diagnosis code(s).");
        System.out.println("Updated " + revCount  + " existing codes updated.");
        System.out.println("unRev: " + unRevCount);
       //System.out.println("More than one Rev: " + moreThanOneRev);
        // System.out.println("More than one unRev: " + moreThanOneunRev);
    }

    public void loadICD10(HashMap<String, String> dxCodesMap)
    {
        if (!initOk) return;
        try {
                String dxCode, desc;
                Statement stmt = db.createStatement();
                ResultSet rs = stmt.executeQuery("select dx_code, description from diagnosis_code" );
                while (rs.next()) {
                    desc = rs.getString("description");
                    dxCode = rs.getString("dx_code");
                    String dxCodeNoDot = dxCode.replace(".", "");
                    dxCodesMap.put(dxCode, desc);
                    dxCodesMap.put(dxCodeNoDot, desc);
                }
                stmt.close();

        } catch (SQLException e) {
            throw new RuntimeException("Load ICD10 dx cdoes failed: " + e.getMessage());
        }
    }

    public void cleanup()
    {
        try {
            if (initOk) db.close();
        } catch (Exception e) {
            // Do nothing
        }
    }


}
