#!/bin/sh
SCHEMA=$1
PW=$2

if [ -z "${SCHEMA}" ] || [ -z "${PW}" ];  then
	echo "Usage: ./run.sh <schema> <password> (i.e.  ./run.sh ag_test password)"
	exit 1
fi
java -jar dx_codes_import-1.0-jar-with-dependencies.jar -u root -p "${PW}" -s ${SCHEMA} -i icd10cm-tabular-2022.xml 
